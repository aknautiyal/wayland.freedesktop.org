��S      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`7d25d9e`�h]�h �	reference���)��}�(h�git commit 7d25d9e�h]�h �Text����git commit 7d25d9e�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/7d25d9e�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4b9dfe32f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�h]�h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4b9dfe32f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _absolute_coordinate_ranges:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��absolute-coordinate-ranges�uh0h]h:Kh hhhh8�F/home/whot/code/libinput/build/doc/user/absolute-coordinate-ranges.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�#Coordinate ranges for absolute axes�h]�h�#Coordinate ranges for absolute axes�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hX�  libinput requires that all touchpads provide a correct axis range and
resolution. These are used to enable or disable certain features or adapt
the interaction with the touchpad. For example, the software button area is
narrower on small touchpads to avoid reducing the interactive surface too
much. Likewise, palm detection works differently on small touchpads as palm
interference is less likely to happen.�h]�hX�  libinput requires that all touchpads provide a correct axis range and
resolution. These are used to enable or disable certain features or adapt
the interaction with the touchpad. For example, the software button area is
narrower on small touchpads to avoid reducing the interactive surface too
much. Likewise, palm detection works differently on small touchpads as palm
interference is less likely to happen.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��Touchpads with incorrect axis ranges generate error messages
in the form:
<blockquote>
Axis 0x35 value 4000 is outside expected range [0, 3000]
</blockquote>�h]�h��Touchpads with incorrect axis ranges generate error messages
in the form:
<blockquote>
Axis 0x35 value 4000 is outside expected range [0, 3000]
</blockquote>�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hXF  This error message indicates that the ABS_MT_POSITION_X axis (i.e. the x
axis) generated an event outside the expected range of 0-3000. In this case
the value was 4000.
This discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends can be the source of a number of perceived
bugs in libinput.�h]�hXF  This error message indicates that the ABS_MT_POSITION_X axis (i.e. the x
axis) generated an event outside the expected range of 0-3000. In this case
the value was 4000.
This discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends can be the source of a number of perceived
bugs in libinput.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�#.. _absolute_coordinate_ranges_fix:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�absolute-coordinate-ranges-fix�uh0h]h:K h hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�$Measuring and fixing touchpad ranges�h]�h�$Measuring and fixing touchpad ranges�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh�)��}�(h� To fix the touchpad you need to:�h]�h� To fix the touchpad you need to:�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K!h h�hhubh �enumerated_list���)��}�(hhh]�(h �	list_item���)��}�(h�0measure the physical size of your touchpad in mm�h]�h�)��}�(hh�h]�h�0measure the physical size of your touchpad in mm�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K#h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�run touchpad-edge-detector�h]�h�)��}�(hh�h]�h�run touchpad-edge-detector�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K$h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�.trim the udev match rule to something sensible�h]�h�)��}�(hj  h]�h�.trim the udev match rule to something sensible�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K%h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�Preplace the resolution with the calculated resolution based on physical settings�h]�h�)��}�(hj%  h]�h�Preplace the resolution with the calculated resolution based on physical settings�����}�(hj%  h j'  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K&h j#  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�test locally�h]�h�)��}�(hj<  h]�h�test locally�����}�(hj<  h j>  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K'h j:  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�$send a patch to the systemd project
�h]�h�)��}�(h�#send a patch to the systemd project�h]�h�#send a patch to the systemd project�����}�(hjW  h jU  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K(h jQ  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��enumtype��arabic��prefix�h�suffix��.�uh0h�h h�hhh8hkh:K#ubh�)��}�(h� Detailed explanations are below.�h]�h� Detailed explanations are below.�����}�(hjv  h jt  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K*h h�hhubh�)��}�(hX~  `libevdev <http://freedesktop.org/wiki/Software/libevdev/>`_ provides a tool
called **touchpad-edge-detector** that allows measuring the touchpad's input
ranges. Run the tool as root against the device node of your touchpad device
and repeatedly move a finger around the whole outside area of the
touchpad. Then control+c the process and note the output.
An example output is below:�h]�(h)��}�(h�<`libevdev <http://freedesktop.org/wiki/Software/libevdev/>`_�h]�h�libevdev�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��libevdev��refuri��.http://freedesktop.org/wiki/Software/libevdev/�uh0hh j�  ubh^)��}�(h�1 <http://freedesktop.org/wiki/Software/libevdev/>�h]�h!}�(h#]��libevdev�ah%]�h']��libevdev�ah)]�h+]��refuri�j�  uh0h]�
referenced�Kh j�  ubh� provides a tool
called �����}�(h� provides a tool
called �h j�  hhh8Nh:Nubh �strong���)��}�(h�**touchpad-edge-detector**�h]�h�touchpad-edge-detector�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubhX   that allows measuring the touchpad’s input
ranges. Run the tool as root against the device node of your touchpad device
and repeatedly move a finger around the whole outside area of the
touchpad. Then control+c the process and note the output.
An example output is below:�����}�(hX   that allows measuring the touchpad's input
ranges. Run the tool as root against the device node of your touchpad device
and repeatedly move a finger around the whole outside area of the
touchpad. Then control+c the process and note the output.
An example output is below:�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K,h h�hhubh �literal_block���)��}�(hX/  $> sudo touchpad-edge-detector /dev/input/event4
Touchpad SynPS/2 Synaptics TouchPad on /dev/input/event4
Move one finger around the touchpad to detect the actual edges
Kernel says:       x [1024..3112], y [2024..4832]
Touchpad sends:    x [2445..4252], y [3464..4071]

Touchpad size as listed by the kernel: 49x66mm
Calculate resolution as:
   x axis: 2088/<width in mm>
   y axis: 2808/<height in mm>

Suggested udev rule:
# <Laptop model description goes here>
evdev:name:SynPS/2 Synaptics TouchPad:dmi:bvnLENOVO:bvrGJET72WW(2.22):bd02/21/2014:svnLENOVO:pn20ARS25701:pvrThinkPadT440s:rvnLENOVO:rn20ARS25701:rvrSDK0E50512STD:cvnLENOVO:ct10:cvrNotAvailable:*
 EVDEV_ABS_00=2445:4252:<x resolution>
 EVDEV_ABS_01=3464:4071:<y resolution>
 EVDEV_ABS_35=2445:4252:<x resolution>
 EVDEV_ABS_36=3464:4071:<y resolution>�h]�hX/  $> sudo touchpad-edge-detector /dev/input/event4
Touchpad SynPS/2 Synaptics TouchPad on /dev/input/event4
Move one finger around the touchpad to detect the actual edges
Kernel says:       x [1024..3112], y [2024..4832]
Touchpad sends:    x [2445..4252], y [3464..4071]

Touchpad size as listed by the kernel: 49x66mm
Calculate resolution as:
   x axis: 2088/<width in mm>
   y axis: 2808/<height in mm>

Suggested udev rule:
# <Laptop model description goes here>
evdev:name:SynPS/2 Synaptics TouchPad:dmi:bvnLENOVO:bvrGJET72WW(2.22):bd02/21/2014:svnLENOVO:pn20ARS25701:pvrThinkPadT440s:rvnLENOVO:rn20ARS25701:rvrSDK0E50512STD:cvnLENOVO:ct10:cvrNotAvailable:*
 EVDEV_ABS_00=2445:4252:<x resolution>
 EVDEV_ABS_01=3464:4071:<y resolution>
 EVDEV_ABS_35=2445:4252:<x resolution>
 EVDEV_ABS_36=3464:4071:<y resolution>�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��	xml:space��preserve�uh0j�  h:K;h h�hhh8hkubh�)��}�(hX|  Note the discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends.
To fix the advertised ranges, the udev rule should be taken and trimmed
before being sent to the `systemd project <https://github.com/systemd/systemd>`_.
An example commit can be found
`here <https://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045>`_.�h]�(h��Note the discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends.
To fix the advertised ranges, the udev rule should be taken and trimmed
before being sent to the �����}�(h��Note the discrepancy between the coordinate range the kernels advertises vs.
what the touchpad sends.
To fix the advertised ranges, the udev rule should be taken and trimmed
before being sent to the �h j�  hhh8Nh:Nubh)��}�(h�7`systemd project <https://github.com/systemd/systemd>`_�h]�h�systemd project�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��systemd project�j�  �"https://github.com/systemd/systemd�uh0hh j�  ubh^)��}�(h�% <https://github.com/systemd/systemd>�h]�h!}�(h#]��systemd-project�ah%]�h']��systemd project�ah)]�h+]��refuri�j�  uh0h]j�  Kh j�  ubh�!.
An example commit can be found
�����}�(h�!.
An example commit can be found
�h j�  hhh8Nh:Nubh)��}�(h�\`here <https://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045>`_�h]�h�here�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]��name��here�j�  �Rhttps://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045�uh0hh j�  ubh^)��}�(h�U <https://github.com/systemd/systemd/commit/26f667eac1c5e89b689aa0a1daef6a80f473e045>�h]�h!}�(h#]��here�ah%]�h']��here�ah)]�h+]��refuri�j  uh0h]j�  Kh j�  ubh�.�����}�(hjs  h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KKh h�hhubh�)��}�(h��In most cases the match can and should be trimmed to the system vendor (svn)
and the product version (pvr), with everything else replaced by a wildcard
(*). In this case, a Lenovo T440s, a suitable match string would be:
::�h]�h��In most cases the match can and should be trimmed to the system vendor (svn)
and the product version (pvr), with everything else replaced by a wildcard
(*). In this case, a Lenovo T440s, a suitable match string would be:�����}�(h��In most cases the match can and should be trimmed to the system vendor (svn)
and the product version (pvr), with everything else replaced by a wildcard
(*). In this case, a Lenovo T440s, a suitable match string would be:�h j,  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KRh h�hhubj�  )��}�(h�Gevdev:name:SynPS/2 Synaptics TouchPad:dmi:*svnLENOVO:*pvrThinkPadT440s*�h]�h�Gevdev:name:SynPS/2 Synaptics TouchPad:dmi:*svnLENOVO:*pvrThinkPadT440s*�����}�(hhh j;  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:K\h h�hhh8hkubh �note���)��}�(h��hwdb match strings only allow for alphanumeric ascii characters. Use a
wildcard (* or ?, whichever appropriate) for special characters.�h]�h�)��}�(h��hwdb match strings only allow for alphanumeric ascii characters. Use a
wildcard (* or ?, whichever appropriate) for special characters.�h]�h��hwdb match strings only allow for alphanumeric ascii characters. Use a
wildcard (* or ?, whichever appropriate) for special characters.�����}�(hjQ  h jO  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KZh jK  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jI  h h�hhh8hkh:Nubh�)��}�(h�*The actual axis overrides are in the form:�h]�h�*The actual axis overrides are in the form:�����}�(hje  h jc  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K]h h�hhubj�  )��}�(h�;# axis number=min:max:resolution
 EVDEV_ABS_00=2445:4252:42�h]�h�;# axis number=min:max:resolution
 EVDEV_ABS_00=2445:4252:42�����}�(hhh jq  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:Kfh h�hhh8hkubh�)��}�(h�7or, if the range is correct but the resolution is wrong�h]�h�7or, if the range is correct but the resolution is wrong�����}�(hj�  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kdh h�hhubj�  )��}�(h�-# axis number=::resolution
 EVDEV_ABS_00=::42�h]�h�-# axis number=::resolution
 EVDEV_ABS_00=::42�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:Kmh h�hhh8hkubh�)��}�(h��Note the leading single space. The axis numbers are in hex and can be found
in *linux/input-event-codes.h*. For touchpads ABS_X, ABS_Y,
ABS_MT_POSITION_X and ABS_MT_POSITION_Y are required.�h]�(h�ONote the leading single space. The axis numbers are in hex and can be found
in �����}�(h�ONote the leading single space. The axis numbers are in hex and can be found
in �h j�  hhh8Nh:Nubh �emphasis���)��}�(h�*linux/input-event-codes.h*�h]�h�linux/input-event-codes.h�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�S. For touchpads ABS_X, ABS_Y,
ABS_MT_POSITION_X and ABS_MT_POSITION_Y are required.�����}�(h�S. For touchpads ABS_X, ABS_Y,
ABS_MT_POSITION_X and ABS_MT_POSITION_Y are required.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Klh h�hhubjJ  )��}�(h��The touchpad's ranges and/or resolution should only be fixed when
there is a significant discrepancy. A few units do not make a
difference and a resolution that is off by 2 or less usually does
not matter either.�h]�h�)��}�(h��The touchpad's ranges and/or resolution should only be fixed when
there is a significant discrepancy. A few units do not make a
difference and a resolution that is off by 2 or less usually does
not matter either.�h]�h��The touchpad’s ranges and/or resolution should only be fixed when
there is a significant discrepancy. A few units do not make a
difference and a resolution that is off by 2 or less usually does
not matter either.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kph j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jI  h h�hhh8hkh:Nubh�)��}�(hXY  Once a match and override rule has been found, follow the instructions at
the top of the
`60-evdev.hwdb <https://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb>`_
file to save it locally and trigger the udev hwdb reload. Rebooting is
always a good idea. If the match string is correct, the new properties will
show up in the
output of�h]�(h�YOnce a match and override rule has been found, follow the instructions at
the top of the
�����}�(h�YOnce a match and override rule has been found, follow the instructions at
the top of the
�h j�  hhh8Nh:Nubh)��}�(h�T`60-evdev.hwdb <https://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb>`_�h]�h�60-evdev.hwdb�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��60-evdev.hwdb�j�  �Ahttps://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb�uh0hh j�  ubh^)��}�(h�D <https://github.com/systemd/systemd/blob/master/hwdb/60-evdev.hwdb>�h]�h!}�(h#]��
evdev-hwdb�ah%]�h']��60-evdev.hwdb�ah)]�h+]��refuri�j�  uh0h]j�  Kh j�  ubh��
file to save it locally and trigger the udev hwdb reload. Rebooting is
always a good idea. If the match string is correct, the new properties will
show up in the
output of�����}�(h��
file to save it locally and trigger the udev hwdb reload. Rebooting is
always a good idea. If the match string is correct, the new properties will
show up in the
output of�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kuh h�hhubj�  )��}�(h�$udevadm info /sys/class/input/event4�h]�h�$udevadm info /sys/class/input/event4�����}�(hhh j	  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:K�h h�hhh8hkubh�)��}�(h�tAdjust the command for the event node of your touchpad.
A udev builtin will apply the new axis ranges automatically.�h]�h�tAdjust the command for the event node of your touchpad.
A udev builtin will apply the new axis ranges automatically.�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h h�hhubh�)��}�(h��When the axis override is confirmed to work, please submit it as a pull
request to the `systemd project <https://github.com/systemd/systemd>`_.�h]�(h�WWhen the axis override is confirmed to work, please submit it as a pull
request to the �����}�(h�WWhen the axis override is confirmed to work, please submit it as a pull
request to the �h j%  hhh8Nh:Nubh)��}�(h�7`systemd project <https://github.com/systemd/systemd>`_�h]�h�systemd project�����}�(hhh j.  ubah!}�(h#]�h%]�h']�h)]�h+]��name��systemd project�j�  �"https://github.com/systemd/systemd�uh0hh j%  ubh^)��}�(h�% <https://github.com/systemd/systemd>�h]�h!}�(h#]��id1�ah%]�h']�h)]��systemd project�ah+]��refuri�j>  uh0h]j�  Kh j%  ubh�.�����}�(hjs  h j%  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h h�hhubeh!}�(h#]�(�$measuring-and-fixing-touchpad-ranges�h�eh%]�h']�(�$measuring and fixing touchpad ranges��absolute_coordinate_ranges_fix�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j\  h�s�expect_referenced_by_id�}�h�h�subeh!}�(h#]�(�#coordinate-ranges-for-absolute-axes�hjeh%]�h']�(�#coordinate ranges for absolute axes��absolute_coordinate_ranges�eh)]�h+]�uh0hlh hhhh8hkh:Kj_  }�ji  h_sja  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�au�nameids�}�(ji  hjjh  je  j\  h�j[  jX  j�  j�  j�  j�  j  j  j�  j�  u�	nametypes�}�(ji  �jh  Nj\  �j[  Nj�  �j�  �j  �j�  �uh#}�(hjhnje  hnh�h�jX  h�j�  j�  j�  j�  j  j  j�  j�  jE  j?  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h�)��}�(h�2Duplicate explicit target name: "systemd project".�h]�h�6Duplicate explicit target name: “systemd project”.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�jE  a�level�K�type��INFO��source�hk�line�Kuh0j�  h h�hhh8hkh:K�uba�transform_messages�]�(j�  )��}�(hhh]�h�)��}�(hhh]�h�@Hyperlink target "absolute-coordinate-ranges" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�DHyperlink target "absolute-coordinate-ranges-fix" is not referenced.�����}�(hhh j*  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j'  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�K uh0j�  ube�transformer�N�
decoration�Nhhub.