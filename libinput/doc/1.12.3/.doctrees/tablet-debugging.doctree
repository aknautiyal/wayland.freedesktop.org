���-      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`67b2e32`�h]�h �	reference���)��}�(h�git commit 67b2e32�h]�h �Text����git commit 67b2e32�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/67b2e32�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7fd1528816a8>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7fd1528816a8>�h]�h�<git commit <function get_git_version_full at 0x7fd1528816a8>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7fd1528816a8>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _tablet-debugging:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��tablet-debugging�uh0h]h:Kh hhhh8�</home/whot/code/libinput/build/doc/user/tablet-debugging.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Debugging tablet issues�h]�h�Debugging tablet issues�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh^)��}�(h�.. _tablet-capabilities:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�tablet-capabilities�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Required tablet capabilities�h]�h�Required tablet capabilities�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh �	paragraph���)��}�(h��To handle a tablet correctly, libinput requires a set of capabilities
on the device. When these capabilities are missing, libinput ignores the
device and prints an error to the log. This error messages reads�h]�h��To handle a tablet correctly, libinput requires a set of capabilities
on the device. When these capabilities are missing, libinput ignores the
device and prints an error to the log. This error messages reads�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh �literal_block���)��}�(h�Pmissing tablet capabilities: xy pen btn-stylus resolution. Ignoring this device.�h]�h�Pmissing tablet capabilities: xy pen btn-stylus resolution. Ignoring this device.�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]��	xml:space��preserve�uh0h�h:Kh h�hhh8hkubh�)��}�(h�(or in older versions of libinput simply:�h]�h�(or in older versions of libinput simply:�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(h�Ilibinput bug: device does not meet tablet criteria. Ignoring this device.�h]�h�Ilibinput bug: device does not meet tablet criteria. Ignoring this device.�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�h�h�uh0h�h:Kh h�hhh8hkubh�)��}�(h�hWhen a tablet is rejected, it is usually possible to verify the issue with
the ``libinput record`` tool.�h]�(h�OWhen a tablet is rejected, it is usually possible to verify the issue with
the �����}�(h�OWhen a tablet is rejected, it is usually possible to verify the issue with
the �h h�hhh8Nh:Nubh �literal���)��}�(h�``libinput record``�h]�h�libinput record�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh� tool.�����}�(h� tool.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(hX  **xy** indicates that the tablet is missing the ``ABS_X`` and/or ``ABS_Y``
axis. This indicates that the device is mislabelled and the udev tag
``ID_INPUT_TABLET`` is applied to a device that is not a tablet.
A bug should be filed against `systemd <http://github.com/systemd/systemd>`__.�h]�h�)��}�(hX  **xy** indicates that the tablet is missing the ``ABS_X`` and/or ``ABS_Y``
axis. This indicates that the device is mislabelled and the udev tag
``ID_INPUT_TABLET`` is applied to a device that is not a tablet.
A bug should be filed against `systemd <http://github.com/systemd/systemd>`__.�h]�(h �strong���)��}�(h�**xy**�h]�h�xy�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j
  ubh�* indicates that the tablet is missing the �����}�(h�* indicates that the tablet is missing the �h j
  ubh�)��}�(h�	``ABS_X``�h]�h�ABS_X�����}�(hhh j#  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j
  ubh� and/or �����}�(h� and/or �h j
  ubh�)��}�(h�	``ABS_Y``�h]�h�ABS_Y�����}�(hhh j6  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j
  ubh�F
axis. This indicates that the device is mislabelled and the udev tag
�����}�(h�F
axis. This indicates that the device is mislabelled and the udev tag
�h j
  ubh�)��}�(h�``ID_INPUT_TABLET``�h]�h�ID_INPUT_TABLET�����}�(hhh jI  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j
  ubh�L is applied to a device that is not a tablet.
A bug should be filed against �����}�(h�L is applied to a device that is not a tablet.
A bug should be filed against �h j
  ubh)��}�(h�/`systemd <http://github.com/systemd/systemd>`__�h]�h�systemd�����}�(hhh j\  ubah!}�(h#]�h%]�h']�h)]�h+]��name��systemd��refuri��!http://github.com/systemd/systemd�uh0hh j
  ubh�.�����}�(h�.�h j
  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubj  )��}�(h��**pen** or **btn-stylus** indicates that the tablet does not have the
``BTN_TOOL_PEN`` or ``BTN_STYLUS`` bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�h]�h�)��}�(h��**pen** or **btn-stylus** indicates that the tablet does not have the
``BTN_TOOL_PEN`` or ``BTN_STYLUS`` bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�h]�(j  )��}�(h�**pen**�h]�h�pen�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubh� or �����}�(h� or �h j�  ubj  )��}�(h�**btn-stylus**�h]�h�
btn-stylus�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubh�- indicates that the tablet does not have the
�����}�(h�- indicates that the tablet does not have the
�h j�  ubh�)��}�(h�``BTN_TOOL_PEN``�h]�h�BTN_TOOL_PEN�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh� or �����}�(hj�  h j�  ubh�)��}�(h�``BTN_STYLUS``�h]�h�
BTN_STYLUS�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�� bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�����}�(h�� bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K#h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubj  )��}�(hXc  **resolution** indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the `60-evdev.hwdb
<https://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb>`__ file
on your machine and file a pull request with the fixes against
`systemd <https://github.com/systemd/systemd/>`__.�h]�h�)��}�(hXc  **resolution** indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the `60-evdev.hwdb
<https://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb>`__ file
on your machine and file a pull request with the fixes against
`systemd <https://github.com/systemd/systemd/>`__.�h]�(j  )��}�(h�**resolution**�h]�h�
resolution�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubh�� indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the �����}�(h�� indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the �h j�  ubh)��}�(h�U`60-evdev.hwdb
<https://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb>`__�h]�h�60-evdev.hwdb�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��60-evdev.hwdb�jl  �Ahttps://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb�uh0hh j�  ubh�E file
on your machine and file a pull request with the fixes against
�����}�(h�E file
on your machine and file a pull request with the fixes against
�h j�  ubh)��}�(h�1`systemd <https://github.com/systemd/systemd/>`__�h]�h�systemd�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]��name��systemd�jl  �#https://github.com/systemd/systemd/�uh0hh j�  ubh�.�����}�(hjr  h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K'h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0h�h8hkh:Kh h�hhubeh!}�(h#]�(�required-tablet-capabilities�h�eh%]�h']�(�required tablet capabilities��tablet-capabilities�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j>  h�s�expect_referenced_by_id�}�h�h�subeh!}�(h#]�(�debugging-tablet-issues�hjeh%]�h']�(�debugging tablet issues��tablet-debugging�eh)]�h+]�uh0hlh hhhh8hkh:KjA  }�jK  h_sjC  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�js  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�au�nameids�}�(jK  hjjJ  jG  j>  h�j=  j:  u�	nametypes�}�(jK  �jJ  Nj>  �j=  Nuh#}�(hjhnjG  hnh�h�j:  h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "tablet-debugging" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�9Hyperlink target "tablet-capabilities" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ube�transformer�N�
decoration�Nhhub.